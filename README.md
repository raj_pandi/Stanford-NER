# Stanford-NER
Our java package called Check is used for finding top 7 topics from data collected, it is built on Stanford Named Entity Recognizer.

- Stanford Named Entity Recognizer: http://nlp.stanford.edu/software/CRF-NER.shtml
- It contains 7 classifiers: Organization, Person, Location, Date, Time, Money and Percent.

Input : Raw data in any format.

Output: The code outputs a text file by name Top10entities that contains classified words grouped by aforementioned top 7 classifiers